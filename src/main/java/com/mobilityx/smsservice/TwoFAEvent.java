package com.mobilityx.smsservice;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class TwoFAEvent {

    @JsonProperty("contact_number")
    private String contactNumber;

    @JsonProperty("2fa_message")
    private String twoFAMessage;
}
