package com.mobilityx.smsservice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.SubscribableChannel;
import reactor.core.scheduler.Schedulers;

@EnableBinding(SubscribeListener.SubscribeSmsSink.class)
@Slf4j
@Configuration
public class SubscribeListener {

    private final AwsService awsService;
    private final String topicArn;

    public SubscribeListener(AwsService awsService, AppConfig appConfig) {
        this.awsService = awsService;
        this.topicArn = appConfig.getTopicArn();
    }

    @StreamListener("subscribe-sink")
    public void subscribeToSmsNotification(User user) {
        log.info("Consuming user: {}", user);
        awsService.subscribeSnsTopic(user.getContactNumber(), topicArn)
                  .subscribeOn(Schedulers.elastic())
                  .subscribe();
    }

    public interface SubscribeSmsSink {
        @Input("subscribe-sink")
        SubscribableChannel subscribeSmsSink();
    }
}
