package com.mobilityx.smsservice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;

@EnableEurekaClient
@SpringBootApplication
@Slf4j
public class SmsServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmsServiceApplication.class, args);
    }

    @StreamListener("errorChannel")
    public void error(Message<?> err) {
        log.error("Error while consuming message: {}", err);
    }
}
