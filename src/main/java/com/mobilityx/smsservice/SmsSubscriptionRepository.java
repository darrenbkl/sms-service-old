package com.mobilityx.smsservice;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.r2dbc.repository.query.Query;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface SmsSubscriptionRepository extends R2dbcRepository<SmsSubscription, Integer> {

    @Query("SELECT * FROM sms_subscriptions WHERE phone_number = $1 and topic = $2")
    Flux<SmsSubscription> findByContactNumberAndTopicArn(String contactNumber, String topicArn);

    @Query("DELETE FROM sms_subscriptions WHERE phone_number = $1 and topic = $2")
    Flux<SmsSubscription> deleteByContactNumberAndTopicArn(String contactNumber, String topicArn);
}