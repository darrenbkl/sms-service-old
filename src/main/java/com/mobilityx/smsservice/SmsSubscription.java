package com.mobilityx.smsservice;

import lombok.Value;
import lombok.experimental.Wither;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table("sms_subscriptions")
@Value
@Wither
public class SmsSubscription {

    @Column("phone_number")
    String phoneNumber;

    @Column("topic")
    String topic;

    @Column("subscription_id")
    String subscriptionId;
}