package com.mobilityx.smsservice;

import lombok.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsAsyncClient;

@Value
@ConfigurationProperties(prefix = "aws")
public class AppConfig {

    private String accessKeyId;
    private String secretAccessKey;
    private String region;
    private String topicArn;

    @Bean
    AwsBasicCredentials awsBasicCredentials() {
        return AwsBasicCredentials.create(accessKeyId, secretAccessKey);
    }

    @Bean
    SnsAsyncClient snsAsyncClient(AwsBasicCredentials awsBasicCredentials) {
        Region awsRegion = Region.of(this.region);
        return SnsAsyncClient.builder()
                             .region(awsRegion)
                             .credentialsProvider(StaticCredentialsProvider.create(awsBasicCredentials))
                             .build();
    }
}
