package com.mobilityx.smsservice;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class User {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("contact_number")
    private String contactNumber;
}
