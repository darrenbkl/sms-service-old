package com.mobilityx.smsservice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.SubscribableChannel;
import reactor.core.scheduler.Schedulers;

@EnableBinding(SendSmsListener.SendSmsSink.class)
@Slf4j
@Configuration
public class SendSmsListener {

    private final AwsService awsService;
    private final String topicArn;

    public SendSmsListener(AwsService awsService, AppConfig appConfig) {
        this.awsService = awsService;
        this.topicArn = appConfig.getTopicArn();
    }

    @StreamListener("send-sms-sink")
    public void sendSms(TwoFAEvent event) {
        log.info("Consuming event: {}", event);
        awsService.sendDirectSms(event.getContactNumber(), event.getTwoFAMessage())
                  .subscribeOn(Schedulers.elastic())
                  .subscribe();
    }

    public interface SendSmsSink {
        @Input("send-sms-sink")
        SubscribableChannel sendSmsSink();
    }
}
