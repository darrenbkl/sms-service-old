package com.mobilityx.smsservice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import software.amazon.awssdk.services.sns.SnsAsyncClient;
import software.amazon.awssdk.services.sns.model.*;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class AwsService {

    private final SmsSubscriptionRepository smsSubscriptionRepository;
    private final SnsAsyncClient snsAsyncClient;

    public AwsService(SmsSubscriptionRepository smsSubscriptionRepository, SnsAsyncClient snsAsyncClient) {
        this.smsSubscriptionRepository = smsSubscriptionRepository;
        this.snsAsyncClient = snsAsyncClient;
    }

    //=====================================================================
    // Publish direct SMS to phone number
    //=====================================================================

    public Mono<PublishResponse> sendDirectSms(String phoneNumber, String message) {
        log.info("AwsService.sendSMS");

        Map<String, MessageAttributeValue> smsAttributes = new HashMap<>();
        smsAttributes.put("AWS.SNS.SMS.SMSType", MessageAttributeValue.builder()
                                                                      .stringValue("Transactional")
                                                                      .dataType("String")
                                                                      .build());

        final PublishRequest publishRequest = PublishRequest.builder()
                                                            .message(message)
                                                            .phoneNumber(phoneNumber)
                                                            .messageAttributes(smsAttributes)
                                                            .build();

        return Mono.defer(() -> Mono.fromFuture(snsAsyncClient.publish(publishRequest)))
                   .doOnNext(res -> log.info("AWS SNS PublishResponse: {}", res))
                   .doOnError(err -> log.error("AWS SNS publish error: ", err))
                   .retryBackoff(2, Duration.ofSeconds(3));
    }

    //=====================================================================
    // Subscribe user to SMS notification
    //=====================================================================

    public Mono<SmsSubscription> subscribeSnsTopic(String contactNumber, String topicArn) {
        log.info("AwsService.subscribeSnsTopic");

        return this.subscribeArnTopic("sms", topicArn, contactNumber)
                   .map(subscriptionId -> new SmsSubscription(contactNumber, topicArn, subscriptionId))
                   .flatMap(smsSubscriptionRepository::save);
    }

    private Mono<String> subscribeArnTopic(String protocol, String topicArn, String contactNumber) {
        log.info("AwsService.subscribeArnTopic");

        final SubscribeRequest subscribeRequest = SubscribeRequest.builder()
                                                                  .protocol(protocol)
                                                                  .topicArn(topicArn)
                                                                  .endpoint(contactNumber)
                                                                  .returnSubscriptionArn(true)
                                                                  .build();

        return Mono.defer(() -> Mono.fromFuture(snsAsyncClient.subscribe(subscribeRequest)))
                   .map(SubscribeResponse::subscriptionArn)
                   .doOnNext(res -> log.info("Response from AWS SNS Subscription: {}", res))
                   .doOnError(err -> log.error("Error from AWS SNS Subscription: ", err));
        // TODO retry
    }


    //=====================================================================
    // Unsubscribe user from SMS notification
    //=====================================================================

    /*
    1. get all smsSubscription where topic= and contact=
    2. for each of these smsSubscription, call aws to unsub them by subscrpitionID
    3. delete these smsSubscription
     */

    public Flux<SmsSubscription> unsubscribeSnsTopic(String contactNumber, String topicArn) {
        log.info("AwsService.unsubscribeSnsTopic");

        return smsSubscriptionRepository.findByContactNumberAndTopicArn(contactNumber, topicArn)
                                        .doOnNext(sub -> log.info("Removing sms subscriptions: {}", sub))
                                        .flatMap(sub -> Mono.just(sub.getSubscriptionId()))
                                        .flatMap(this::unsubscribeArnTopic)
                                        .thenMany(smsSubscriptionRepository.deleteByContactNumberAndTopicArn(contactNumber, topicArn));
    }

    private Mono<String> unsubscribeArnTopic(String subscriptionArn) {
        log.info("AwsService.unsubscribeArnTopic");

        final UnsubscribeRequest unsubscribeRequest = UnsubscribeRequest.builder()
                                                                        .subscriptionArn(subscriptionArn)
                                                                        .build();

        return Mono.defer(() -> Mono.fromFuture(snsAsyncClient.unsubscribe(unsubscribeRequest)))
                   .doOnError(err -> log.error("Error from AWS SNS Subscription: ", err))
                   .map(UnsubscribeResponse::toString);
    }
}
