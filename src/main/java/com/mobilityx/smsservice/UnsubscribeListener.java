package com.mobilityx.smsservice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.SubscribableChannel;
import reactor.core.scheduler.Schedulers;

@EnableBinding(UnsubscribeListener.UnsubscribeSmsSink.class)
@Slf4j
@Configuration
public class UnsubscribeListener {

    private final AwsService awsService;
    private final String topicArn;

    public UnsubscribeListener(AwsService awsService, AppConfig appConfig) {
        this.awsService = awsService;
        this.topicArn = appConfig.getTopicArn();
    }

    @StreamListener("unsubscribe-sink")
    public void unsubscribeToSmsNotification(User user) {
        log.info("Consuming user: {}", user);
        awsService.unsubscribeSnsTopic(user.getContactNumber(), topicArn)
                  .subscribeOn(Schedulers.elastic())
                  .subscribe();
    }

    public interface UnsubscribeSmsSink {
        @Input("unsubscribe-sink")
        SubscribableChannel unsubscribeSmsSink();
    }
}
