import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.sns.SnsAsyncClient


// TODO restrict getter setter
//@Configuration
//@ConfigurationProperties(prefix = "aws")
//data class AppConfigKotlin(val accessKeyId: String,
//                           val secretAccessKey: String,
//                           val region: String,
//                           val smsArnTopic: String) {
//    @Bean
//    internal fun awsBasicCredentials(): AwsBasicCredentials {
//        return AwsBasicCredentials.create(accessKeyId, secretAccessKey)
//    }
//
//    @Bean
//    internal fun snsAsyncClient(awsBasicCredentials: AwsBasicCredentials): SnsAsyncClient {
//        val awsRegion = Region.of(this.region)
//        return SnsAsyncClient.builder()
//                .region(awsRegion)
//                .credentialsProvider(StaticCredentialsProvider.create(awsBasicCredentials))
//                .build()
//    }
//}